# FileWatcher POC

# References

- [ASP.NET Core BackgroundService](https://docs.microsoft.com/en-us/dotnet/standard/microservices-architecture/multi-container-microservice-net-applications/background-tasks-with-ihostedservice#implementing-ihostedservice-with-a-custom-hosted-service-class-deriving-from-the-backgroundservice-base-class)
- [ASP.NET Core Hosted Service](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/host/hosted-services?view=aspnetcore-2.1)
- [File Providers](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/file-providers)
- [Recurring](https://stackoverflow.com/a/34764822/6445037)
- [FileSystemWatcher not working](https://stackoverflow.com/a/240008/6445037)

# Dockerize

```
docker build -t ert-file-monitor .
docker run -it --rm -v ${pwd}/temp_data:/app/temp_data --name ert-file-monitor ert-file-monitor
```