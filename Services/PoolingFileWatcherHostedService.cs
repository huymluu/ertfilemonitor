﻿using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ErtFileMonitor.Services
{
    public class PoolingFileWatcherHostedService : IHostedService
    {
        private string monitoringPath;
        private Dictionary<String, FileInfo> files = new Dictionary<string, FileInfo>();

        public PoolingFileWatcherHostedService()
        {
            monitoringPath = Directory.GetCurrentDirectory() + "/temp_data";
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine("PoolingFileWatcherHostedService - Monitoring path: " + monitoringPath);

            StartTimer();

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        private void OnFileChanged(string filePath)
        {
            // TODO
        }

        private void StartTimer()
        {
            var myTimer = new System.Timers.Timer();
            myTimer.Elapsed += (source, e) => { RefreshFileInfos(true); };
            myTimer.Interval = 1000;
            myTimer.Enabled = true;
        }

        private void RefreshFileInfos(bool triggerEvent)
        {

            foreach (string filePath in Directory.GetFiles(monitoringPath))
            {
                FileInfo fileInfo = new FileInfo(filePath);

                // Check if file is new or changed
                bool fileChanged = false;
                if (files.ContainsKey(filePath))
                {
                    FileInfo oldFileInfo = files.GetValueOrDefault(filePath);
                    if (!oldFileInfo.LastWriteTime.Equals(fileInfo.LastWriteTime))
                    {
                        fileChanged = true; // changed exist file
                        Console.WriteLine("RefreshFileInfos - file changed: " + filePath);
                    }
                } else
                {
                    fileChanged = true; // new file
                    Console.WriteLine("RefreshFileInfos - new file: " + filePath);
                }

                // Update
                files[filePath] = fileInfo;

                // Trigger event
                if (fileChanged && triggerEvent)
                {
                    OnFileChanged(filePath);
                }
            }

            // Remove deleted files
            foreach (string filePath in files.Keys.ToList())
            {
                if (!File.Exists(filePath))
                {
                    Console.WriteLine("RefreshFileInfos - file deleted: " + filePath);
                    files.Remove(filePath);
                }
            }
        }

    }
}
