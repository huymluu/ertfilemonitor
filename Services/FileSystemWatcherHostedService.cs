﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;

namespace ErtFileMonitor.Services
{
    public class FileSystemWatcherHostedService : IHostedService
    {
        private FileSystemWatcher fileSystemWatcher;
        private string monitoringPath;

        public FileSystemWatcherHostedService()
        {
            monitoringPath = Directory.GetCurrentDirectory() + "/temp_data";
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            fileSystemWatcher = new FileSystemWatcher();

            fileSystemWatcher.Created += FileSystemWatcher_Created;
            fileSystemWatcher.Changed += FileSystemWatcher_Changed;
            fileSystemWatcher.Deleted += FileSystemWatcher_Deleted;
            fileSystemWatcher.Renamed += FileSystemWatcher_Renamed;

            fileSystemWatcher.Path = monitoringPath;

            fileSystemWatcher.EnableRaisingEvents = true;

            Console.WriteLine("FileSystemWatcherHostedService - Monitoring path: " + monitoringPath);

            // StartTimer();

            return Task.CompletedTask;
        }

        private void StartTimer()
        {
            var myTimer = new System.Timers.Timer();
            myTimer.Elapsed += new ElapsedEventHandler(ListFiles);
            myTimer.Interval = 5000;
            myTimer.Enabled = true;
        }

        private void ListFiles(object source, ElapsedEventArgs e)
        {
            string[] files = Directory.GetFiles(monitoringPath);
            Console.WriteLine("List files:");
            foreach (string file in files)
            {
                Console.WriteLine(file);
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            fileSystemWatcher.Dispose();

            return Task.CompletedTask;
        }

        private static void FileSystemWatcher_Renamed(object sender, RenamedEventArgs e)
        {
            Console.WriteLine($"FileSystemWatcherHostedService - File renamed from {e.OldName} to {e.Name}");
        }

        private static void FileSystemWatcher_Deleted(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine($"FileSystemWatcherHostedService - File deleted - {e.Name}");
        }

        private static void FileSystemWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine($"FileSystemWatcherHostedService - File changed - {e.Name}");
        }

        private static void FileSystemWatcher_Created(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine($"FileSystemWatcherHostedService - File created - {e.Name}");
        }
    }
}
